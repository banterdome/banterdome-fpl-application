import { LeaguePosition } from "./leaguePosition";

export interface LeagueResponse
{
    info: LeaguePosition[]
}