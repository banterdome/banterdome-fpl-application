export interface LeaguePosition{
    name: string;
    overallPoints: number;
    gameweekPoints: number;
    hit: number;
}
