import { CommonModule } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CoreModule } from "../core/core.module";
import { LeagueComponent } from "./components/league.component";
import { LeagueService } from "./services/league.service";

@NgModule({
    declarations: [
      LeagueComponent
    ],
    imports: [CoreModule, CommonModule],
    providers: [LeagueService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class LeagueModule {}
  