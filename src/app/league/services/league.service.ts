import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/api.service';
import { ConfigService } from 'src/app/core/config.service';
import { LeagueResponse } from '../resources/leagueResponse';

@Injectable()
export class LeagueService {

    constructor(private configService: ConfigService, private apiService: ApiService) {}

    get(): Observable<LeagueResponse>{

        return this.apiService.get(
            `${this.configService.config.banterdomeFplApiUrl}/league/getLiveLeague/${this.configService.config.banterdomeLeague}`);
    }
}