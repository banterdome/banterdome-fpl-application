import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LeaguePosition } from '../resources/leaguePosition';
import { LeagueService } from '../services/league.service';

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.scss']
})
export class LeagueComponent implements OnInit {

  public league: LeaguePosition[] = []; 

  constructor(private router: Router, private leagueService: LeagueService) { }

  ngOnInit(): void {

    
    this.leagueService.get()
            .subscribe((response) => {
                this.league = response.info
            }, () => {
                console.log("hello");
            })
  }

  onSwipeRight(){
    this.router.navigate(['/'])
  }
}
