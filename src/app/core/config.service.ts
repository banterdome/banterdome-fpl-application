import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService {
  private readonly configPath = '../../assets/config/config.json';

  config: any;

  loadConfiguration(): Promise<any> {
    return fetch(this.configPath)
      .then((res) => res.json())
      .then((config) => {
        this.config = config;
      });
  }
}