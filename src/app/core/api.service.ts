import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http'
import { QueryParameter } from "./resources/queryParameter";

@Injectable()
export class ApiService{

    constructor(private httpClient: HttpClient) {}

    public get<TResponse>(url: string, parameters?: QueryParameter[]): Observable<TResponse> {
        let params = new HttpParams();

        if(parameters !== undefined){
            parameters.forEach(parameter => {
                params.append(parameter.name, parameter.value)
            });
        }
       
        return this.httpClient.get<TResponse>(url)
    }
}