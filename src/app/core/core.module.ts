import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { ApiService } from "./api.service";
import { ConfigService } from "./config.service";

@NgModule({
    declarations: [],
    imports: [],
    providers: [ConfigService, ApiService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule {}