export interface QueryParameter{
    name: string;
    value: any;
}